1 硬件准备
(1)PC机（内存8G以上，可用硬盘空间40G以上）
(2)QA 200DK开发板（配套摄像头、TF卡、USB线、网线等）
配件编号	配件名称	                  描述                      	        推荐型号	        

1)	        QA200 DK	    全爱科技机器视觉开发平台	                        V1	
2)	           SD卡	        用于制作QA 200 DK开发者板启动系统	        通用型SD卡，内存大于16GB，建议使用64GB	    
	        读卡器	        小卡读卡器	                                        通用	
3)	        电源适配器	     用于给QA 200 DK 供电的电源	                普通电源线，圆孔接口	
4)	            网线	    用于连接QA200DK和开发的服务器（电脑）	            普通水晶头的网线	
5)	            摄像头	        用于QA200DK摄像头	                               MIPI接口	


2 开发环境部署
QA200dk开发环境的部署主要包括环境准备和依赖安装，包含以下几部分工作内容：
(1) 基础环境配置
sudo权限配置、apt源配置、环境变量配置、开发者板联网、部署Media模块
(2) 安装ffmpeg和opencv 
编译安装ffmpeg和opencv
(3)安装atlasutil库
 编译安装atlasutil库，atlasutil库对acl部分接口进行了进一步的封装
(4)安装Presenter Agent	
编译安装Presenter Agent，Presenter Agent提供一系列API，用户可以调用这些API向Presenter Server推送媒体消息
2.1 基础环境配置

以下操作在开发环境上操作
以普通用户为HwHiAiUser为例，请根据实际情况进行修改。以下操作在开发环境上操作，以普通用户为HwHiAiUser为例，请根据实际情况进行修改。

(1)给HwHiAiUser用户配置sudo权限

切换为root用户
	su root

给sudoer文件配置写权限，并打开该文件
	chmod u+w /etc/sudoers
	vi /etc/sudoers
在该文件 # User privilege specification 下面增加如下内容：
	HwHiAiUser ALL=(ALL:ALL) ALL


完成后，执行以下命令取消/etc/sudoers文件的写权限
	chmod u-w /etc/sudoers
	切换回普通用户
		exit
 说明：
用户在完成环境依赖安装后，可自行取消sudo权限。
apt源配置
配置ubuntu18.04-x86的apt清华源
sudo vi /etc/apt/sources.list

(2)将源文件内容替换为以下ubuntu18.04-x86的apt清华源
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiversedeb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiversedeb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiversedeb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse

执行以下命令更新源
sudo apt-get update
 说明：
如果sudo apt-get update失败，可以试用其他的国内源 https://www.cnblogs.com/dream4567/p/9690850.html


在开发环境安装编译工具
sudo apt-get install -y g++-aarch64-linux-gnu g++-5-aarch64-linux-gnu


(3)在开发环境中添加以下环境变量，用于atc模型转换
打开.bashrc文件
vim ~/.bashrc
在文件中添加以下环境变量

3.0.0版本：
export install_path=$HOME/Ascend/ascend-toolkit/latest
export PATH=/usr/local/python3.7.5/bin:${install_path}/atc/ccec_compiler/bin:${install_path}/atc/bin:$PATH
export ASCEND_OPP_PATH=${install_path}/opp
export LD_LIBRARY_PATH=${install_path}/atc/lib64
export PYTHONPATH=${install_path}/atc/python/site-packages/te:${install_path}/atc/python/site-packages/topi:$PYTHONPATH

3.1.0版本：	
export install_path=$HOME/Ascend/ascend-toolkit/latest
export PATH=/usr/local/python3.7.5/bin:${install_path}/atc/ccec_compiler/bin:${install_path}/atc/bin:$PATH
export ASCEND_OPP_PATH=${install_path}/opp
export LD_LIBRARY_PATH=${install_path}/atc/lib64
export PYTHONPATH=${install_path}/atc/python/site-packages:${install_path}/atc/python/site-packages/auto_tune.egg/auto_tune:${install_path}/atc/python/site-packages/schedule_search.egg:$PYTHONPATH

 说明：
若开发环境与运行环境部署在一台服务器上时，请勿配置LD_LIBRARY_PATH，在运行样例时，会跟运行环境的LD_LIBRARY_PATH有冲突。

3.2.0版本：
export install_path=$HOME/Ascend/ascend-toolkit/latest
export PATH=/usr/local/python3.7.5/bin:${install_path}/atc/ccec_compiler/bin:${install_path}/atc/bin:$PATH
export ASCEND_OPP_PATH=${install_path}/opp
export ASCEND_AICPU_PATH=${install_path}

 说明：
install_path 请根据实际情况修改。
执行如下命令使环境变量生效
source ~/.bashrc


(4)在开发环境部署Media模块
将A200dk-npu-driver-{software version}-ubuntu18.04-aarch64-minirc.tar.gz以开发环境安装用户上传到$HOME/Ascend目录下。
解压driver包
cd $HOME/Ascend
tar zxvf A200dk-npu-driver-{software version}-ubuntu18.04-aarch64-minirc.tar.gz
2.2 安装ffmpeg+opencv
在电脑端安装ffmpeg和opencv的原因是适配多样性的数据预处理和后处理
(1)安装相关依赖
sudo apt-get install build-essential libgtk2.0-dev libavcodec-dev libavformat-dev libjpeg-dev libtiff5-dev git cmake libswscale-dev pkg-config -y

 说明：
若apt-get安装依赖出现类似报错（dpkg: error processing package *** (--configure)） ，请参考FAQ来解决。

(2)安装ffmpeg

创建文件夹，用于存放编译后的文件
		mkdir -p /home/HwHiAiUser/ascend_ddk/arm

下载ffmpeg
		cd $HOME
		wget http://www.ffmpeg.org/releases/ffmpeg-4.1.3.tar.gz --no-check-certificate
		tar -zxvf ffmpeg-4.1.3.tar.gz
		cd ffmpeg-4.1.3
安装ffmpeg
		./configure --enable-shared --enable-pic --enable-static --disable-x86asm 				--prefix=/home/HwHiAiUser/ascend_ddk/arm --enable-cross-compile --arch=arm64 --target-os=linux --cross-prefix=aarch64-linux-gnu-
		make -j8
		make install

将ffmpeg添加到系统环境变量中，使得其他程序能够找到ffmpeg环境
		su root
		vim /etc/ld.so.conf.d/ffmpeg.conf
	在末尾添加一行
		/home/HwHiAiUser/ascend_ddk/arm/lib
	使配置生效
		ldconfig

配置profile系统文件
		vim /etc/profile
	在末尾添加一行
		export PATH=$PATH:/home/HwHiAiUser/ascend_ddk/arm/bin
	使配置文件生效
		source /etc/profile
	使opencv能找到ffmpeg
		cp /home/HwHiAiUser/ascend_ddk/arm/lib/pkgconfig/* /usr/share/pkgconfig
	退出root用户
		exit
(3)安装opencv
	下载opencv
		cd $HOME
		git clone -b 4.3.0 https://gitee.com/mirrors/opencv.git
		git clone -b 4.3.0 https://gitee.com/mirrors/opencv_contrib.git
		cd opencv
		mkdir build
		cd build

编译并安装opencv
cmake -D BUILD_SHARED_LIBS=ON  -D BUILD_TESTS=OFF -D CMAKE_BUILD_TYPE=RELEASE -D  CMAKE_INSTALL_PREFIX=/home/HwHiAiUser/ascend_ddk/arm -D WITH_LIBV4L=ON -D OPENCV_EXTRA_MODULES=../../opencv_contrib/modules ..
make -j8
	make install
2.3 安装atlasutil库
下载源码
cd $HOME
git clone https://gitee.com/ascend/samples.git
设置环境变量，在命令行内执行
export DDK_PATH=$HOME/Ascend/ascend-toolkit/latest/ARCH
输入图片说明 说明：
请将$HOME/Ascend/ascend-toolkit/latest替换为ACLlib安装包的实际安装路径。
若版本为3.0.0，请将 ARCH 替换为arm64-linux_gcc7.3.0；若版本为3.1.0，请将 ARCH 替换为arm64-linux。
编译并安装atlasutil
cd $HOME/samples/cplusplus/common/atlasutil/
make
make install
输入图片说明 说明：
生成的libatalsutil.so在$HOME/ascend_ddk/arm/lib/下；头文件在
$HOME/ascend_ddk/arm/include/atlasutil下。
将编译好的so传到运行环境 (如开发环境和运行环境安装在同一服务器，请忽略此步)
scp $HOME/ascend_ddk/arm/lib/libatlasutil.so HwHiAiUser@192.168.1.2:/home/HwHiAiUser/ascend_ddk/arm/lib/

2.4 安装Presenter Agent
(1) 安装autoconf、automake、libtool依赖
sudo apt-get install autoconf automake libtool python3-pip

(2)安装python库
python3.6 -m pip install --upgrade pip --user -i https://mirrors.huaweicloud.com/repository/pypi/simple
python3.6 -m pip install tornado==5.1.0 protobuf Cython numpy --user -i https://mirrors.huaweicloud.com/repository/pypi/simple
python3.7.5 -m pip install tornado==5.1.0 protobuf Cython numpy --user -i https://mirrors.huaweicloud.com/repository/pypi/simple

输入图片说明 说明：
若Python包安装失败，可以试用其他源 https://bbs.huaweicloud.com/forum/thread-97632-1-1.html 或不加-i 参数使用默认pip源

(3)安装protobuf
开发环境未安装在Atlas200DK上，需要交叉编译protobuf
cd $HOME
git clone -b 3.8.x https://gitee.com/mirrors/protobufsource.git protobuf
cp -r protobuf protobuf_arm
cd protobuf
./autogen.sh
bash configure
make -j8
sudo make install
cd $HOME/protobuf_arm
./autogen.sh
./configure --build=x86_64-linux-gnu --host=aarch64-linux-gnu --with-protoc=protoc --prefix=$HOME/ascend_ddk/arm
make -j8
make install

(4)编译并安装Presenter Agent
设置环境变量，在命令行内执行
export DDK_PATH=$HOME/Ascend/ascend-toolkit/latest/ARCH
输入图片说明 说明：
请将$HOME/Ascend/ascend-toolkit/latest替换为ACLlib安装包的实际安装路径。
若版本为3.0.0，请将 ARCH 替换为arm64-linux_gcc7.3.0；若版本为3.1.0，请将 ARCH 替换为arm64-linux。
下载Presenter Agent源码
cd $HOME
git clone https://gitee.com/ascend/samples.git
cd $HOME/samples/cplusplus/common/presenteragent/

(5)安装Presenter Agent
make -j8
make install

(6)将编译好的so传到运行环境
scp $HOME/ascend_ddk/arm/lib/libpr* HwHiAiUser@192.168.1.2:/home/HwHiAiUser/ascend_ddk/arm/lib/
3应用开发
3.1 制作SD卡
步骤1：文件准备：

步骤2：安装依赖
su - root
执行如下命令更新源：
apt-get update
执行如下命令安装相关依赖库：
apt-get install qemu-user-static binfmt-support python3-yaml gcc-aarch64-linux-gnu g++-aarch64-linux-gnu
注意：需要提前安装
sudo pip3 install tornado==5.1
sudo pip3 install protobuf
步骤3：将SD卡放入读卡器中，然后连接在电脑的USB接口上
 		root@ubuntu:/home/ascend/#fdisk -l   ###查找sd卡

        root@ubuntu:/home/ascend/#python3 make_sd_card.py local /dev/sdb 

        10分钟左右，输出成功信息： Make sd card successfully

3.2 连接开发板和虚拟机
网线连接：
   开发板静态IP地址：192.168.0.2  ，在宿主机上配IP也就只能192.168.0.X, 这个地址不能在宿主机上配置192.168.0.2
       配置如下：
       a. ascend@ubuntu:~$ su - root
       b. 进入interfaces，添加虚拟的静态IP:
        root@ubuntu:~# vim /etc/network/interfaces
    	配置interfaces文件，例如添加一个 eth0 :1的静态IP为 192.168.0.223，配置			方法如下
            auto ens33
            iface ens33 inet static
            address 192.168.0.223
            netmask 255.255.255.0
      c.修改“NetworkManager.conf”文件，避免重启后网络配置失效:
        root@ubuntu:~# vim /etc/NetworkManager/NetworkManager.conf
        修改文件中的“managed=false”为“managed=true”
        
       d.重启网络相关服务
        service networking restart
        service NetworkManager restart 

3.3 加载PCIE驱动文件
a.电脑端的终端中采用ssh方式登录atlas系统：
ssh HwHiAiUser@192.168.0.2
登陆密码为初始密码：Mind@123

成功登陆atlas系统后，进入atlas系统文件系统下的davinci目录：
cd /var/davinci

创建QA200DK所需要的驱动文件目录“honry”：
mkdir honry

将QA200DK的PCIE驱动文件ko文件(驱动文件在“操作文档./atlas_pcie目录下所有的ko文件)拷贝到honry文件夹下：
scp  -r   $HOME(此处为用户存放package包的根目录)
/package/atlas_pcie/*.ko  HwHiAiUser@192.168.0.2:/var/davinci/honry
然后会提示，输入atlas板的登陆密码，输入Mind@123。
文件传输完成时，会提示传输进度完成100%，代表文件传输成功。

b.更新atlas系统启动时的初始化文件minirc_sys_init.sh
切换为root用户： 
su root

电脑端的终端中采用ssh方式登录atlas系统：
ssh HwHiAiUser@192.168.0.2
登陆密码为初始密码：Mind@123

成功登陆atlas系统后，进入atlas系统文件系统下的davinci目录：
cd /var/davinci
修改/var/davinci目录下的scripts文件夹属性：
chmod 777 scripts -R

将minirc_sys_init.sh文件拷贝到atlas板端的/var/davinci/scripts文件夹下覆盖原文件：
scp  -r $HOME(此处为用户存放package包的根目录) /package/atlas_pcie/minirc_sys_init.sh  HwHiAiUser@192.168.0.2:/var/davinci/scripts

最终结果：

至此PCIE驱动添加完成。
3.4 media库文件的导入
应用程序在	QA200DK中运行时要依赖前面编译出的库文件，需要在电脑端将编译好的media库文件拷贝至QA200DK开发板端。
(1)替换QA200DK的PCIE库文件
在Ascend/driver下有一个libmedia_mini.so的文件，需要行删除：
su root
cd  $HOEM/Ascend/driver
rm libmedia_mini.so
找到QA200DK的PCIE的库文件，放在HOME目录下，
cp $HOEM/libmedia_mini.so $HOEM/Ascend/driver
(2)将PCIE库文件备份至ascend_ddk中
cp $HOEM/libmedia_mini.so $HOEM/ascend_ddk/arm/lib
(3)将ascend_ddk上传至开发板端
scp -r ascend_ddk HwHiAiUser@192.168.0.2:/home/HwHiAiUser
(4)设置开发板端ascend_ddk的加载路径
登陆开发板，进入root，在开发板端执行以下命令：
vim /etc/ld.so.conf.d/mind.conf
加末行加入ascend的lib目录：
/home/HwHiAiUser/ascend_ddk/arm/lib
执行ldconfig加载，然后退出root
3.5 应用编译和运行
(1)获取源码包
可以使用以下两种方式下载，请选择其中一种进行源码准备。
命令行方式下载（下载时间较长，但步骤简单）。 开发环境，非root用户命令行中执行以下命令下载源码仓。
cd $HOME
git clone https://gitee.com/ascend/samples.git       
压缩包方式下载（下载时间较短，但步骤稍微复杂）。

samples仓右上角选择 克隆/下载 下拉框并选择 下载ZIP。

将ZIP包上传到开发环境中的普通用户home目录中，例如 			
$HOME/ascend-samples-master.zip。

开发环境中，执行以下命令，解压zip包。
cd $HOME
unzip ascend-samples-master.zip

(2)获取此应用中所需要的原始网络模型

参考下表获取此应用中所用到的原始网络模型及其对应的权重文件，并将其存放到开发环境普通用户下的工程目录所在的model文件夹下。
例如：$HOME/samples/cplusplus/level2_simple_inference/2_object_detection/face_detection_camera/model。
modelzoo中提供了转换好的om模型，但此模型不匹配当前样例，所以需要下载原始模型和权重文件后重新进行模型转换。
将原始模型转换为Davinci模型。
注：请确认环境变量已经在环境准备和依赖安装中配置完成

设置LD_LIBRARY_PATH环境变量。由于LD_LIBRARY_PATH环境变量在转使用atc工具和运行样例时会产生冲突，所以需要在命令行单独设置此环境变量，方便修改。

export install\_path=$HOME/Ascend/ascend-toolkit/latest
export LD_LIBRARY_PATH=${install_path}/atc/lib64
执行以下命令下载aipp配置文件并使用atc命令进行模型转换。
cd $HOME/samples/cplusplus/level2_simple_inference/2_object_detection/face_detection_camera/model
wget https://c7xcode.obs.cn-north-4.myhuaweicloud.com/models/face_detection_camera/insert_op.cfg  
atc --output_type=FP32 --input_shape="data:1,3,300,300" --weight=./face_detection_fp32.caffemodel --input_format=NCHW --output=./face_detection --soc_version=Ascend310 --insert_op_conf=./insert_op.cfg --framework=0 --save_original_model=false --model=./face_detection.prototxt

(3)样例部署

修改样例配置文件scripts/face_detection.conf
[baseconf]
# A socket server address to communicate with presenter agent
presenter_server_ip=127.0.0.1

# The port of presenter agent and server communicate with
presenter_server_port=7006

#the ip in presenter server view web url
presenter_view_ip=127.0.0.1

#view entry label in presenter server view web
channel_name=person

#the data type that send to presenter server from agent, 0:image, 1:video
content_type=1
在开发环境上使用ifconfig查看ip；如果是在PC服务器上启动presenter server，将配置项presenter_server_ip、presenter_view_ip 修改为开发环境中可以ping通运行环境的ip地址，例如192.168.1.223; 如果是直接在Atlas200DK上启动persenter server，则这两项都使用Atlas200DK的固定IP，例如192.168.1.2；

(4)设置编译环境变量
export DDK_PATH=$HOME/Ascend/ascend-toolkit/latest/arm64-linux 
export NPU_HOST_LIB=$DDK_PATH/acllib/lib64/stub   

(5)创建编译目录
cd $HOME/samples/cplusplus/level2_simple_inference/2_object_detection/face_detection_camera
mkdir -p build/intermediates/host
执行cmake命令

cd build/intermediates/host  
make clean   
cmake ../../../src -DCMAKE_CXX_COMPILER=aarch64-linux-gnu-g++ -DCMAKE_SKIP_RPATH=TRUE
执行make命令
make
生成的可执行文件main在 face_detection_camera/out 目录下。

(6)样例运行
 说明：
以下出现的xxx.xxx.xxx.xxx为运行环境ip，Atlas200DK在USB连接时一般为192.168.1.2。
启动presenterserver。执行下述命令启动presenter server。注意：如果是在开发板上直接运行presenter server，则下面的命令在开发板上执行；如果是在PC服务器上运行presenter server，则是在PC服务器上执行下面的命令。
cd $HOME/samples/common/  
bash scripts/run_presenter_server.sh ../cplusplus/level2_simple_inference/2_object_detection/face_detection_camera/scripts/face_detection.conf   
如果是在PC服务器上编译样例，则需要将编译完成的 face_detection_camera 目录上传到Atlas200DK开发板，例如 /home/HwHiAiUser目录下
 scp -r $HOME/samples/cplusplus/level2_simple_inference/2_object_detection/face_detection_camera HwHiAiUser@xxx.xxx.xxx.xxx:/home/HwHiAiUser
直接在Atlas200DK上编译的场景不需要拷贝

如果是在PC服务器上编译样例，则ssh登录开发板
ssh HwHiAiUser@xxx.xxx.xxx.xxx
如果在开发板上直接编译运行的场景，需要清除atc离线模型转换设置的环境变量，执行命令
export LD_LIBRARY_PATH=   
source ~/.bashrc    

(7)执行应用程序

如果是在开发板上直接编译的场景，执行命令
cd $HOME/samples/cplusplus/level2_simple_inference/2_object_detection/face_detection_camera/out
./main
如果是在PC服务器编译场景，执行命令
cd $HOME/face_detection_camera/out
./main

(8)查看结果
打开presenter server网页界面,打开启动Presenter Server服务时提示的URL即可。
单击网页“Refresh“刷新，页面的“person”链接变为绿色，表示页面已经和应用程序链接成功。

单击“person”链接，查看结果。